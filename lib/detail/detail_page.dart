import 'dart:io';
import 'package:film_project_final/user_repository.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:film_project_final/authentication/authentication.dart';
import 'package:film_project_final/services/movie_services.dart';
import 'package:film_project_final/models/movie.dart';


class DetailPage extends StatefulWidget {
  final UserRepository userRepository;
  final Movie movie;

  DetailPage({Key key, @required this.userRepository, @required this.movie})
      : super(key: key);

  _MyDetailPageState createState() {
    return _MyDetailPageState();
  }
}

class _MyDetailPageState extends State<DetailPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(25, 255, 218, 1),
      appBar:AppBar(
        elevation: 0,
        title: RichText(
          text: TextSpan(
            children: <TextSpan>[

              TextSpan(text: widget.movie.title, style: TextStyle(fontWeight: FontWeight.w300, fontSize: 20),),
            ],
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 30.0,
              color: Colors.white,
            ),
          ),
        ),
        centerTitle: false,
        backgroundColor: Color.fromRGBO(25, 255, 218, 1),
      ),
      body: SafeArea(
        child: Container(
          child: SingleChildScrollView(
            child: Stack(
              children: <Widget>[
                Image.network(
                  'http://image.tmdb.org/t/p/w500/${widget.movie.poster_path}',

                ),
              ],
            ),
          )
        ),

      ),
    );
  }

}