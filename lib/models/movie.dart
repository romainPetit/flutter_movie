import 'dart:convert';

Movie movieFromJson(String str) {
  final jsonData = json.decode(str);
  return Movie.fromJson(jsonData);
}


List<Movie> allMoviesFromJson(String str) =>
    json.decode(str)["results"].map<Movie>((x) {
      return Movie.fromJson(x);
    }).toList();


class Movie {
  int id;
  String title;
  String overview;
  String poster_path;
  double vote_average;

  Movie({
    this.id,
    this.title,
    this.overview,
    this.poster_path,
    this.vote_average,
  });

  factory Movie.fromJson(Map<String, dynamic> json) => new Movie(
        id: json["id"],
        title: json["title"],
        overview: json["overview"],
        poster_path: json["poster_path"],
        vote_average: parseAverage(json["vote_average"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "title": title,
        "overview": overview,
        "poster_path": poster_path,
        "vote_average": vote_average,
      };

  @override
  String toString() {
    return 'Movie{id: $id, title: $title, overview: $overview, poster_path: $poster_path, vote_average: $vote_average}';
  }

  static parseAverage(final avg) => avg.runtimeType == double ? avg : avg + .0;
}
