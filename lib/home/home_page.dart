import 'dart:io';
import 'package:film_project_final/detail/detail_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:film_project_final/authentication/authentication.dart';
import 'package:film_project_final/services/movie_services.dart';
import 'package:film_project_final/models/movie.dart';


import 'dart:typed_data';

//import "package:flare_flutter/flare_actor.dart";


//import 'package:my_emojy/enableUpload/enableUpload.dart';

class HomePage extends StatefulWidget {
  _MyHomePageState createState() {
    return _MyHomePageState();
  }
}

class _MyHomePageState extends State<HomePage> {
  AuthenticationBloc authenticationBloc;
  File _sampleImage;
  bool visibleAddButton = true;
  double opacity = 1;

  @override
  Widget build(BuildContext context) {
    authenticationBloc = BlocProvider.of<AuthenticationBloc>(context);

    return Scaffold(
      backgroundColor: Color.fromRGBO(25, 255, 218, 1),
      appBar:AppBar(
        elevation: 0,
        title: RichText(
          text: TextSpan(
            children: <TextSpan>[
              TextSpan(text: "My"),
              TextSpan(text: "Movies", style: TextStyle(fontWeight: FontWeight.w300),),
            ],
            style: TextStyle(
              fontWeight: FontWeight.w500,
              fontSize: 30.0,
              color: Colors.white,
            ),
          ),
        ),
        centerTitle: false,
        backgroundColor: Color.fromRGBO(25, 255, 218, 1),
        actions: <Widget>[
          IconButton(
            onPressed: _logout,
            icon: Icon(Icons.account_circle),
          ),
        ],

      ),
      body: SafeArea(
        child:Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: MediaQuery.of(context).size.height / 3,
          child: _buildList(context)
        ),
      ),
      floatingActionButton: Visibility(
        child: FloatingActionButton(
          backgroundColor: Color.fromRGBO(223, 63, 0, 1),
          child: Icon(Icons.add),
          onPressed:
          _toggleAddButton,
        ),
        visible: visibleAddButton,
      ),
    );
  }

  @override
  Widget _buildList(BuildContext context) {
    return FutureBuilder<List<Movie>>(
      future: getAllNowPlayingMovies(),
      builder: (context, snapshot){
        switch (snapshot.connectionState) {
          case ConnectionState.done:
            if(snapshot.hasError){
              return Text("Error");
            }
            return ListView.builder(
              itemCount: snapshot.data.length,
              // This next line does the trick.
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return GestureDetector(
                  behavior: HitTestBehavior.translucent,
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailPage(
                          userRepository: authenticationBloc.userRepository,
                          movie: snapshot.data[index],
                        ),
                      ),
                    );
                    },
                  child:Container(
                    width: 160,
                  padding: const EdgeInsets.all(10.0),
                  child: Image.network(
                    'http://image.tmdb.org/t/p/w500/${snapshot.data[index].poster_path}',

                  ),
                  ),
                );
              }
            );
          case ConnectionState.waiting:
            return new Center(child: new CircularProgressIndicator());
          default:
            return Text("default");
        //return new ListView(children: getExpenseItems(snapshot));
        }

      },

    );
  }



  _toggleAddButton() {
    setState(() {
      visibleAddButton = !visibleAddButton;
    });
  }
  _logout() {
    try {
      authenticationBloc.userRepository.logOut();
      authenticationBloc.dispatch(LoggedOut());
    } catch (error) {
      print(error.toString());
    }
  }
}
