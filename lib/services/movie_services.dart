import 'package:http/http.dart' as http;
import 'dart:async';
import 'package:film_project_final/models/movie.dart';
import 'dart:io';

String url = 'https://api.themoviedb.org/3';
String token = '?api_key=94d7ca17ec53ce3d4b53b53452df36c4&language=fr-FR';

Future<List<Movie>> getAllNowPlayingMovies() async {
  final response = await http.get(url+ '/movie/now_playing'+token);
  print(response.body+'\n');
  return allMoviesFromJson(response.body);
}

Future<Movie> getMovie(id) async{
  final response = await http.get('$url/movie/$id'+ token);
  print(response.body+'\n');
  return movieFromJson(response.body);
}